!===============================================================================
 PROGRAM main
!-------------------------------------------------------------------------------
! A program to generate molecular coordinates
!-------------------------------------------------------------------------------
 USE m_var
 USE m_util
 IMPLICIT NONE
!-------------------------------------------------------------------------------
!
! ... Read command line arguments
!
 CALL read_arg
!
! ... Initialize
!
 CALL initialize
!
! ... Generate atomic coordinates
!
 CALL molgen
!
! ... Output atomic coordinates
!
 CALL output
!
! ... Finalize
!
 CALL finalize
!-------------------------------------------------------------------------------
 END PROGRAM
!===============================================================================
