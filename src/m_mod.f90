 MODULE m_var
 !
 IMPLICIT NONE
 ! ... Program name
 CHARACTER(LEN=80) :: prog = 'mogen'
 ! ... Number of atoms in a molecule
 INTEGER :: natm
 ! ... Atomic positions in the cartesian coordinates
 REAL(KIND=8), ALLOCATABLE :: car(:,:)
 ! ... Atomic symbol
 CHARACTER(LEN=2), ALLOCATABLE :: at(:)
 ! ... Bond length of diatomic molecule
 REAL(KIND=8) :: d0, d2
 ! ... Bond length in Angstrom
 REAL(KIND=8) :: d_hh = 0.74d0 ! Expt. (Angstrom)
 REAL(KIND=8) :: d_oo = 1.20d0 ! Expt. (Angstrom)
 REAL(KIND=8) :: d_oo_ooh = 1.34264, & ! PBE (Angstrom)
                 d_oh_ooh = 0.99731    ! PBE (Angstrom)
 REAL(KIND=8) :: d_oh_oh = 0.97969  ! Guess from H2O (Angstrom)
 REAL(KIND=8) :: d_oh_h2o = 0.97969 ! PBE (Angstrom)
 ! ... Bond angle
 REAL(KIND=8) :: t_ooh_ooh = 104.8966 ! PBE (degree)
 REAL(KIND=8) :: t_oh_oh   =   0.0000 ! PBE (degree)
 REAL(KIND=8) :: t_hoh_h2o = 103.6421 ! PBE (degree)
 ! ... INDEX for atom/molecule
 ! ... Angle and translation
 REAL(KIND=8) :: tx = 0.d0, ty = 0.d0, tz = 0.d0
 REAL(KIND=8) :: tr(3)
 ! ... Molecular species
 INTEGER :: imol
 CHARACTER(LEN=80) :: mol = ''
 INTEGER, PARAMETER :: H = 1, H2 = 11, O = 6, OH = 61, O2 = 66, &
                       OOH = 661, H2O = 116, H2O2 = 1166
 ! ... Constants
 REAL(KIND=8) :: pi, tpi, bohr2ang
 ! ... Fortran I/O
 INTEGER :: stdin = 05, stdout = 06, stderr = 00
 INTEGER :: input = 10, output = 20 
 ! ... output format
 INTEGER :: fmt = 1
 ! fmt = 1 : XYZ
 !       2 : STATE
 !
 END MODULE m_var
 !
 MODULE m_util
 IMPLICIT NONE
 !
 CONTAINS
 !
 SUBROUTINE read_arg
 !
 ! ... Read the command line arguments
 !
 USE m_var
 IMPLICIT NONE
 INTEGER :: iarg, iargc, narg
 INTEGER :: ios
 CHARACTER(LEN=256) :: arg, arg2
 narg = iargc()
 IF( narg == 0 )THEN
   CALL usage
 ENDIF
 iarg = 0
 DO WHILE ( iarg < narg )
   iarg = iarg + 1
   CALL getarg( iarg, arg ) 
   SELECT CASE( TRIM( arg ) )
     CASE( '-H' )
       imol = H
       mol = 'H'
     CASE( '-H2' )
       imol = H2
       mol = 'H2'
     CASE( '-O' )
       imol = O
       mol = 'O'
     CASE( '-O2' )
       imol = O2
       mol = 'O2'
     CASE( '-OH' )
       imol = OH
       mol = 'OH'
     CASE( '-H2O' )
       imol = H2O
       mol = 'H2O'
     CASE( '-OOH' )
       imol = OOH
       mol = 'OOH'
     CASE( '-rot-x' )
       iarg = iarg + 1
       CALL getarg( iarg, arg2 ) 
       READ( arg2, * ) tx
     CASE( '-rot-y' )
       iarg = iarg + 1
       CALL getarg( iarg, arg2 ) 
       READ( arg2, * ) ty
     CASE( '-rot-z' )
       iarg = iarg + 1
       CALL getarg( iarg, arg2 ) 
       READ( arg2, * ) tz
     CASE( '-trans-x' )
       iarg = iarg + 1
       CALL getarg( iarg, arg2 ) 
       READ( arg2, * ) tr(1)
     CASE( '-trans-y' )
       iarg = iarg + 1
       CALL getarg( iarg, arg2 ) 
       READ( arg2, * ) tr(2)
     CASE( '-trans-z' )
       iarg = iarg + 1
       CALL getarg( iarg, arg2 ) 
       READ( arg2, * ) tr(3)
     CASE( '-trans' )
       iarg = iarg + 1
       CALL getarg( iarg, arg2 ) 
       READ( arg2, * ) tr(1)
       iarg = iarg + 1
       CALL getarg( iarg, arg2 ) 
       READ( arg2, * ) tr(2)
       iarg = iarg + 1
       CALL getarg( iarg, arg2 ) 
       READ( arg2, * ) tr(3)
     CASE( '-xyz' )
       fmt = 1
     CASE( '-state-legacy', '-state-old' )
       fmt = 2
     CASE( '-pw', '-qe' )
       fmt = 3
     CASE ( '-h' )
       CALL usage
   END SELECT
 ENDDO 
 END SUBROUTINE
 !
 SUBROUTINE usage
 !
 ! Print out the usages
 !
 USE m_var
 IMPLICIT NONE
 WRITE(stderr,'(A,A,A,A,A)')&
 'Usage: ',&
&TRIM(prog),&
&' -[MOL]',&
&' [-trans x y z] [-trans-x x] [-trans-y y] [-trans-z z]',&
&' [-rot-x angle_x] [-rot-y angle_x] [-rot-z angle_z]'
 WRITE(stderr,'(A,A)')&
&'Available molecule [MOL]:',&
&' H, H2, O, O2, OH, H2O, OOH'
 STOP
 END SUBROUTINE
 !
 SUBROUTINE molgen
 !
 ! ... Generate molecular geometry
 !
 USE m_var
 IMPLICIT NONE
 REAL(KIND=8) :: theta
 !
 SELECT CASE( imol )
   CASE(H)
     natm = 1
   CASE(O)
     natm = 1
   CASE(H2)
     natm = 2
   CASE(O2)
     natm = 2
   CASE(OH)
     natm = 2
   CASE(H2O)
     natm = 3
   CASE(OOH)
     natm = 3
   CASE(H2O2)
     natm = 4
 END SELECT
 ALLOCATE( car(3,natm) )
 ALLOCATE( at(natm) )
 car(:,:) = 0.D0
 SELECT CASE( imol )
   CASE(H)
     at(1) = 'H'
   CASE(O)
     at(1) = 'O'
   CASE(H2)
     car(1,1) = -0.5D0 * d_hh
     car(1,2) =  0.5D0 * d_hh
     at(:) = 'H'
   CASE(O2)
     car(1,1) = -0.5D0 * d_oo
     car(1,2) =  0.5D0 * d_oo
     at(:) = 'O'
   CASE(OH)
     car(1,2) = d_oh_oh
     at(1) = 'O'
     at(2) = 'H'
   CASE(H2O)
     theta = t_hoh_h2o / 180.D0 * pi
     car(1,1) = d_oh_h2o * COS( theta / 2.D0 )
     car(2,1) = d_oh_h2o * SIN( theta / 2.D0 )
     car(1,2) =  car(1,1)
     car(2,2) = -car(2,1)
     at(1) = 'H'
     at(2) = 'H'
     at(3) = 'O'
   CASE(OOH)
     theta = t_ooh_ooh / 180.D0 * pi
     car(1,2) = d_oo_ooh
     car(1,3) = d_oo_ooh + d_oh_ooh * cos( pi - theta )
     car(3,3) = d_oh_ooh * sin( pi - theta )
     at(1) = 'O'
     at(2) = 'O'
     at(3) = 'H'
   CASE(H2O2)
     car(1,1) =  0.7247
     car(2,1) =  0.0000
     car(3,1) =  0.0000
     car(1,2) = -0.7247
     car(2,2) =  0.0000
     car(3,2) =  0.0000
     car(1,3) =  0.8233
     car(2,3) = -0.7000
     car(3,3) = -0.6676
     car(1,4) = -0.8233
     car(2,4) = -0.6175
     car(3,4) =  0.7446  
     at(1) = 'O'
     at(2) = 'O'
     at(3) = 'H'
     at(4) = 'H'
 END SELECT
 !
 tx = tx / 180.D0 * pi
 ty = ty / 180.D0 * pi
 tz = tz / 180.D0 * pi
 !
 IF( ABS(tx) > 0 )THEN
   CALL rotx( car, natm, tx ) 
 ENDIF
 IF( ABS(ty) > 0 )THEN
   CALL roty( car, natm, ty ) 
 ENDIF
 IF( ABS(tz) > 0 )THEN
   CALL rotz( car, natm, tz ) 
 ENDIF
 IF( ABS(tr(1))+ABS(tr(2))+ABS(tr(3)) /= 0.D0 ) THEN
   CALL shiftxyz( car, natm, tr ) 
 ENDIF
 !
 END SUBROUTINE 
 !
 SUBROUTINE output
 !
 ! ... Output atomic coordinates
 !
 USE m_var, ONLY: fmt
 IMPLICIT NONE
 IF ( fmt == 1 ) THEN
   CALL xyzgen
 ENDIF
 END SUBROUTINE
 ! 
 SUBROUTINE xyzgen
 !
 ! ... Generate an XYZ file
 !
 USE m_var
 IMPLICIT NONE
 INTEGER :: iatm
 INTEGER :: ierr 
 CHARACTER(LEN=256) :: filen 
 !
 IF( mol == '' )THEN
   filen = 'MOL.xyz'
 ELSE
   filen = TRIM(mol)//'.xyz' 
 ENDIF
 OPEN(UNIT=output,FILE=filen,STATUS='unknown') 
 WRITE(output,'(I5)') natm
 WRITE(output,'(A)') TRIM(mol)
 DO iatm = 1, natm
   WRITE(output,'(A3,3x,3f14.9)')at(iatm), car(1:3,iatm)
 ENDDO
 CLOSE(output)
 !
 END SUBROUTINE
 !
 SUBROUTINE initialize
 !
 ! ... Initialize the variables
 !
 USE m_var
 IMPLICIT NONE
 pi = 4.D0 * ATAN( 1.D0 )
 END SUBROUTINE
 !
 SUBROUTINE finalize
 !
 ! ... Finalize the arrays
 !
 USE m_var
 IF( ALLOCATED( car ) ) DEALLOCATE( car )
 END SUBROUTINE
 !
 SUBROUTINE shiftxyz(cps,natm,t)
 !
 ! ... Shift coordinates
 !
 IMPLICIT NONE
 INTEGER :: iatm, natm
 REAL(KIND=8) :: cps(3,natm),t(3)
 REAL(KIND=8) :: cps_tmp(3)
 DO iatm = 1, natm
   cps(1:3,iatm) = cps(1:3,iatm) + t(1:3)
 ENDDO
 END SUBROUTINE
 !
 SUBROUTINE rotz(cps,natm,ang)
 !
 ! ... Rotate coordinates around z axis
 !
 IMPLICIT NONE
 INTEGER :: iatm, natm
 REAL(KIND=8) :: cps(3,natm)
 REAL(KIND=8) :: ang
 REAL(KIND=8) :: cps_tmp(3)
 REAL(KIND=8) :: cosa, sina
 cosa = COS(ang)
 sina = SIN(ang)
 DO iatm = 1, natm
   cps_tmp(:) = cps(:,iatm)
   cps(1,iatm) = cosa * cps_tmp(1) - sina * cps_tmp(2)
   cps(2,iatm) = sina * cps_tmp(1) + cosa * cps_tmp(2)
 enddo
 END SUBROUTINE
 !
 SUBROUTINE rotx(cps,natm,ang)
 !
 ! ... Rotate coordinates around x axis
 !
 IMPLICIT NONE
 INTEGER :: iatm, natm
 REAL(KIND=8) :: cps(3,natm)
 REAL(KIND=8) :: ang
 REAL(KIND=8) :: cps_tmp(3)
 REAL(KIND=8) :: cosa, sina
 cosa = COS(ang)
 sina = SIN(ang)
 DO iatm = 1, natm
   cps_tmp(:) = cps(:,iatm)
   cps(2,iatm) = cosa * cps_tmp(2) - sina * cps_tmp(3)
   cps(3,iatm) = sina * cps_tmp(2) + cosa * cps_tmp(3)
 enddo
 END SUBROUTINE
 !
 SUBROUTINE roty(cps,natm,ang)
 !
 ! ... Rotate coordinates around y axis
 !
 IMPLICIT NONE
 INTEGER :: iatm, natm
 REAL(KIND=8) :: cps(3,natm)
 REAL(KIND=8) :: ang
 REAL(KIND=8) :: cps_tmp(3)
 REAL(KIND=8) :: cosa, sina
 cosa = COS(ang)
 sina = SIN(ang)
 DO iatm = 1, natm
   cps_tmp(:) = cps(:,iatm)
   cps(3,iatm) = cosa * cps_tmp(3) - sina * cps_tmp(1)
   cps(1,iatm) = sina * cps_tmp(3) + cosa * cps_tmp(1)
 ENDDO
 END SUBROUTINE
 !
 END MODULE
