# MOLGEN

This is a fortran programs to generate structures of small molecules.

## Requirement

- Fortran (f90 or higher) compiler

## Obtaining the program

In an appropriate location, type
```
git clone git@gitlab.com:ikutaro/molgen.git 
```

## Compilation

Go to the source directory:
```
cd molgen/src
```
If gfortran is installed, type
```
make
```
then `molgen` is created.

## Usage

Type
```
molgen -h
```
for the usage.

Currently, H, O, O2, OH, OOH, and H2O2 are implemented.

## Author

Ikutaro Hamada
